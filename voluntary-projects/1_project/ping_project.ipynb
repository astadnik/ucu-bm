{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import numpy as np\n",
    "import matplotlib.pylab as plt\n",
    "import re\n",
    "import subprocess"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gaussian model with known variance :: Bayesian estimation of mean"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Theoretical background\n",
    "\n",
    "Let us assume scalar data $Y_t$ that (approximately) obey a normal (Gaussian) distribution\n",
    "\n",
    "$$\n",
    "Y_k \\sim \\mathcal{N}(\\mu, \\sigma^2), \\qquad \\mu\\in\\mathbb{R}, \\sigma^2 \\in\\mathbb{R}^+,\n",
    "$$\n",
    "\n",
    "where $\\mu$ is the mean value and $\\sigma^2$ it the variance. Assume that $\\sigma^2$ is known and the goal is to sequentially estimate the mean parameter $\\mu$. In statistics, we usually summarize unknown inferred quantities by $\\theta$. Thus, in our example $\\theta\\equiv\\mu$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to evaluate the Bayesian update, we rewrite the model probability density function (pdf) as follows:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "f(y_k|\\theta) = f(y_k|\\mu) &= \\frac{1}{\\sqrt{2\\pi\\sigma^2}} \n",
    "\\exp \\left\\{ \\frac{-1}{2\\sigma^2} (y_k - \\mu)^2 \\right\\} \\\\\n",
    "&= \\frac{1}{\\sqrt{2\\pi\\sigma^2}} \\exp \\left\\{ \\frac{-1}{2\\sigma^2} (y_k^2 - 2y_k\\mu + \\mu^2) \\right\\}.\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Recall that the posterior pdf following from the Bayesian update is analytically tractable if (i) the prior pdf for $\\theta$ is **conjugate** to the model, and (ii) we rewrite the prior and the model into compatible forms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to decide which distribution is **conjugate to the normal (Gaussian) model with known variance** in our example. We can either try own calculations, search in literature, or check [wikipedia](https://en.wikipedia.org/wiki/Conjugate_prior#Table_of_conjugate_distributions).\n",
    "\n",
    "**Result: the conjugate prior for $\\theta\\equiv\\mu$ is another normal (Gaussian) distribution. We denote its mean and variance by $m$ and $s^2$, respectively:**\n",
    "\n",
    "$$\\mu \\sim \\mathcal{N}(m_{k-1}, s_{k-1}^2), \\qquad m\\in\\mathbb{R}, s^2\\in\\mathbb{R}^+.$$ \n",
    "\n",
    "The indices $k-1$ denote that \"at the current instant $k$, we have the information from the beginning instant 0 up to the previous instant $k-1$\". In other words, the prior pdf contains $k-1$ data (measurements, observations...) and we are going to incorporate (assimilate) the $k$th one now. Thus the Bayesian update will transform the prior distribution containing $k-1$ data to the posterior pdf containing $k$ data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Let us now rewrite the prior normal (Gaussian) pdf and the normal (Gaussian) data model pdf into mutually compatible forms. The latter should be characterized by the natural parameter $\\eta$, the sufficient statistic $T(y)$, and a normalizing function $g(\\mu)$. The former should be analytically similar, cf. our lecture notebook. I emphasize, that the prior hyperparameters (here $m$ and $s^2$) will be transformed to parameters $\\nu$ and $\\xi$! The back-transformation is a simple algebra :)**\n",
    "\n",
    "(Remark: in many cases, the forms of the model can be found on [wikipedia](https://en.wikipedia.org/wiki/Exponential_family#Table_of_distributions). It remains to rewrite the prior accordingly.)\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "\\pi(\\mu|\\xi_{k-1}, \\nu_{k-1}) = \\pi(\\mu|m_{k-1}, s_{k-1}^2) &= \\frac{1}{\\sqrt{2\\pi s_{k-1}^2}} \n",
    "\\exp \\left\\{ \\frac{-1}{2 s_{k-1}^2} (\\mu - m_{k-1})^2 \\right\\} \\\\\n",
    "&= \\frac{1}{\\sqrt{2\\pi s_{k-1}^2}} \\exp \\left\\{ \\frac{-1}{2 s_{k-1}^2} (\\mu^2 - 2\\mu m_{k-1} + m_{k-1}^2) \\right\\} \\\\\n",
    "&= \n",
    "\\underbrace{\n",
    "\\frac{1}{\\sqrt{2\\pi s_{k-1}^2}}\\cdot \\exp\\left\\{-\\frac{m_{k-1}^2}{2s_{k-1}^2}\\right\\} \n",
    "}_{q(\\xi_{k-1}, \\nu_{k-1})}\n",
    "\\cdot\n",
    "\\underbrace{1}_{g(\\mu)^{\\nu_{k-1}}}\n",
    "\\exp\\Bigg\\{\n",
    "\\underbrace{\n",
    "\\begin{bmatrix} \\mu \\\\ -\\frac{\\mu^2}{2}\\end{bmatrix}^\\intercal\n",
    "\\begin{bmatrix}\\frac{m_{k-1}}{s_{k-1}^2} \\\\ \\frac{1}{s_{k-1}^2}\\end{bmatrix}\n",
    "}_{\\eta^\\intercal \\xi_{k-1}}\n",
    "\\Bigg\\}.\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the model pdf:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "f(y_k|\\theta) = f(y_k|\\mu) &= \\frac{1}{\\sqrt{2\\pi\\sigma^2}} \n",
    "\\exp \\left\\{ \\frac{-1}{2\\sigma^2} (y_k - \\mu)^2 \\right\\} \\\\\n",
    "&= \\frac{1}{\\sqrt{2\\pi\\sigma^2}} \\exp \\left\\{ \\frac{-1}{2\\sigma^2} (y_k^2 - 2y_k\\mu + \\mu^2) \\right\\} \\\\\n",
    "&= \n",
    "\\underbrace{\n",
    "\\frac{1}{\\sqrt{2\\pi\\sigma^2}} \\cdot \\exp\\left\\{-\\frac{y_k^2}{2\\sigma^2} \\right\\}\n",
    "}_{h(y_k)}\n",
    "\\cdot\n",
    "\\underbrace{1}_{g(\\mu)}\n",
    "\\exp\\Bigg\\{\n",
    "\\underbrace{\n",
    "\\begin{bmatrix} \\mu \\\\ -\\frac{\\mu^2}{2}\\end{bmatrix}^\\intercal\n",
    "\\begin{bmatrix}\\frac{y_{k}}{\\sigma^2} \\\\ \\frac{1}{\\sigma^2}\\end{bmatrix}\n",
    "}_{\\eta^\\intercal T(y_k)}\n",
    "\\Bigg\\}.\n",
    "\\end{aligned}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We already know that one-step update using the Bayes' theorem leads to the update of the prior pdf hyperparameters:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "    \\xi_{k} &= \\xi_{k-1} + T(y_{k}), \\\\\n",
    "    \\nu_{k} &= \\nu_{k-1} + 1.\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Since in our case $g(\\nu) = 1$, we may omit $\\nu$ from calculations. The posterior hyperparameter $\\xi_l$ thus reads\n",
    "\n",
    "$$\n",
    "\\begin{bmatrix}\\frac{m_{k}}{s_{k}^2} \\\\ \\frac{1}{s_{k}^2}\\end{bmatrix}\n",
    "=\n",
    "\\begin{bmatrix}\\frac{m_{k-1}}{s_{k-1}^2} \\\\ \\frac{1}{s_{k-1}^2}\\end{bmatrix}\n",
    "+\n",
    "\\begin{bmatrix}\\frac{y_{k}}{\\sigma^2} \\\\ \\frac{1}{\\sigma^2}\\end{bmatrix}.\n",
    "$$\n",
    "\n",
    "A simple algebra reveals that this update is equivalent to the direct update of the original normal (Gaussian) hyperparameters $m_{k-1}$ a $s_{k-1}^2$,\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "s_k^2 &= \\left( \\frac{1}{s_{k-1}^2} + \\frac{1}{\\sigma^2}\\right)^{-1}, \\\\\n",
    "m_k &= \\left(\\frac{m_{k-1}}{s_{k-1}^2} + \\frac{y_k}{\\sigma^2}\\right) \\cdot s_k^2 = \\frac{\\sigma^2 m_{k-1} + s_{k-1}^2 y_k}{s_{k-1}^2 + \\sigma^2}.\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task: Modeling of ping round trip time using normal (Gaussian) distribution with known variance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code sends 1 ping and waits no more than 15s for pong. The RTT time and its mean deviation (aka standard deviation) is returned.\n",
    "\n",
    "*Remark: the code works exploits unix-type ping. For Windows, please modify it accordingly (I don't have win machine to test it).*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hostname = 'ucu.edu.ua'\n",
    "\n",
    "def ping(hostname, npings=1):\n",
    "    ping = subprocess.Popen([\"ping\", '-c', str(npings), '-w', '15', hostname],\n",
    "                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)\n",
    "    res = str(ping.stdout.readlines()[-1])\n",
    "    #print('Raw output of ping:\\n', res)\n",
    "    tmp = re.split(r'/', res)\n",
    "    rtt = float(tmp[4])\n",
    "    mdev = float(re.split(r' ', tmp[6])[0])\n",
    "    return [rtt, mdev]\n",
    "\n",
    "ping(hostname)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to model the ping RTT using the presented normal (Gaussian) distribution, we need the variance. For training purpose, we just approximate the *standard deviation* (square root of the variance) value by its point estimate. If we wanted to estimate it in the Bayesian way, we would need a more complicated prior distribution. Let us use the following value for the standard deviation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stdev = ping(hostname, npings=10)[1]\n",
    "print(\"Standard deviation: \", stdev)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, your task is to write a code that:\n",
    "1. sequentially sends 100 pings (i.e., $k=1,\\ldots,100$),\n",
    "2. updates the information (the prior pdf) about the mean RTT,\n",
    "3. plots the evolution of the mean RTT estimate for $k=1,\\ldots,100$ with +/- 3 standard deviations of this estimate. Additionally, plot a histogram of absolute frequencies of measured RTT.\n",
    "\n",
    "Set the very initial prior for $\\mu$ to be $\\mathcal{N}(10, 100)$. This corresponds with the idea \"I believe that the mean is 10 and my certainty about this number is expressed by the standard deviation of 10.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "<<CODE HERE>>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results could look like this:\n",
    "\n",
    "![Results](results.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
